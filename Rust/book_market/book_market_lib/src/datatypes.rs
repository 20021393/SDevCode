mod entities {
    
    trait User<I,U,N,P,D,A,B,R> {
        fn user_id(&self) -> I;
        fn login_name(&self) -> U;
        fn full_name(&self) -> N;
        fn password(&self) -> P;
        fn description(&self) -> D;
        fn permissions(&self) -> A;
        fn authorizations(&self) -> B;
        fn reputation(&self) -> R;
    }
}