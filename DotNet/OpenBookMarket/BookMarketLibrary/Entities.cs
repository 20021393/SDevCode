using System.Collections.Generic;
namespace BookMarketLibrary.Entities {
    interface IPair<out A,out B> {
        A Left{get;}B Right{get;}
    }
    interface ITriple<out X,out Y,out Z>:IPair<X,Z> {
        Y Middle{get;}
    }
    interface IUser<out I,out U,out N,out P,out D,out A,out B,out R> {
        I UserId {get;}
        U LoginName {get;}
        N FullName {get;}
        P Password {get;}
        D Description {get;}
        A Permissions {get;}
        B Authorizations {get;}
        R Reputation{get;}
    }
    interface ITransaction<out U,out B,out M,out D,out P> {
        IEnumerable<U> Proposers;
        IEnumerable<ITriple<IEnumerable<U>,B,IEnumerable<U>>> BookTransfers;
        IEnumerable<ITriple<IEnumerable<U>,M,IEnumerable<U>>> MoneyTransfers;
    
    }
}